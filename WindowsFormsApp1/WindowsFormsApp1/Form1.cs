﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        bool turn = true; //true = X, false = O
        int turn_count = 0;

        static string player1, player2;


        public Form1()
        {
            InitializeComponent();
        }

        public static void PlayerNames(string name1, string name2)
        {
            player1 = name1;
            player2 = name2;
        }

        

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
                b.Text = "X";
            else
                b.Text = "O";

            turn = !turn;
            if (turn)
               playing.Text = "It's " + player1 + "'s turn!";
            else playing.Text = "It's " + player2 + "'s turn!";
            b.Enabled = false;
            turn_count++;

            checkForWinner();
        }

        private void checkForWinner()
        {
            bool winner_exists = false;

            //redovi
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                winner_exists = true;
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
                winner_exists = true;
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                winner_exists = true;
            //stupci
            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
                winner_exists = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                winner_exists = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                winner_exists = true;
            //dijagonale
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
                winner_exists = true;
            else if ((C1.Text == B2.Text) && (B2.Text == A3.Text) && (!A3.Enabled))
                winner_exists = true;

            if (winner_exists)
            {
                DisableButtons();


                String winner_is = " ";
                if (turn)
                {
                    winner_is = player2;
                    O_wins.Text = (Int32.Parse(O_wins.Text) + 1).ToString();
                }
                else
                {
                    winner_is = player1;
                    X_wins.Text = (Int32.Parse(X_wins.Text) + 1).ToString();
                }

                MessageBox.Show(winner_is + " Wins!", "Yay!");
            }
            else
            {
                if (turn_count == 9) { 
                    DisableButtons();
                    draw.Text = (Int32.Parse(draw.Text) + 1).ToString();
                    MessageBox.Show("Draw!", "Sorry :(");
                   }
            }

        }//end check

        private void DisableButtons()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }//end foreach
            }
            catch { }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;
            playing.Text = "It's " + player1 + "'s turn!";

            foreach (Control c in Controls)
                {
                    try
                    {
                        Button b = (Button)c;
                        b.Enabled = true;
                        b.Text = "";
                    }
                    catch { }
                }//end foreach
        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                    b.Text = "X";
                else
                    b.Text = "O";
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;
            playing.Text = "It's " + player1 + "'s turn!";
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
                catch { }
            }//end foreach
            O_wins.Text = "0";
            X_wins.Text = "0";
            draw.Text = "0";
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label1.Text = player1;
            label2.Text = player2;
        }
    }
}
